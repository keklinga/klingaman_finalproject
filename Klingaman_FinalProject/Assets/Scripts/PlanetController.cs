﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    
    //list of spawnable planets
    public List<GameObject> spawnPool;
    


    void Start()
    {
        //Calls the method to spawn in the planets
        spawnPlanets();
        
    }

    void Update()
    {
        
    }

    //Method that spawns planets into the game
    void spawnPlanets()
    {
        //stores a random int
        int randomItem = 0;
        //Access the object toSpawn
        GameObject toSpawn;
        //Stores a position
        Vector2 pos;
        //Loops through the amount of planets to spawn
        for (int i = 0; i < GameController.gc.numToSpawn; i++)
        {
            //Sets the random int equal to a random planet within the spawnPool
            randomItem = Random.Range(0, spawnPool.Count);
            //sets the toSpawn object to be that specific planet
            toSpawn = spawnPool[randomItem];
            //stores the minimum spawn point on the x
            float minX = 3f;
            //stores the minimum spawn point on the y
            float minY = 2f;
            //stores the maximum spawn point on the x
            float maxX = 15f;
            //stores the maximum spawn point on the y
            float maxY = 8f;
            //stores the random x coordinate we get from the random.range method
            float useX = Random.Range(minX, maxX);
            //stores the random y coordinate we get from the random.range method
            float useY = Random.Range(minY, maxY);
            //Checks if the random value is greater than .5
            if (Random.value > .5f)
            {
                //Assigns the x coordinate to the negative of that x coordinate
                useX = -useX;

            }
            //Checks if the random value is greater than .5
            if (Random.value > .5f)
            {
                //Assigns the y coordinate to the negative of that y coordinate
                useY = -useY;
            }
            //Assigns the posotion vector to use the x and y coordinates
            pos = new Vector2(useX, useY);
            //Spawns in the planet prefabs according to which planet it is, the posotion, and the rotation
            Instantiate(toSpawn, pos, toSpawn.transform.rotation);

            
            
        }
    }


}
