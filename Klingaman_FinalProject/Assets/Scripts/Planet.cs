﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    //Access the shield gameobject
    public GameObject shield;


    // Start is called before the first frame update
    void Start()
    {
        //Waits a second to check if the shield can be turned on
        Invoke("CheckTurnOn", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Method to check if the shield can be activated
    void CheckTurnOn()
    {
        //Checks if the value generated between 0 and 1 is less than .1
        if(Random.value < .1f)
        {
            //If it is, turn on the shield
            shield.SetActive(true);
            //Turn off the shield after 2.5 seconds
            Invoke("TurnOff", 2.5f);
        } else
        {
            //Otherwise keep checking if the shield can be turned on
            Invoke("CheckTurnOn", 1f);
        }
    }

    //Method to turn off the shield
    void TurnOff()
    {
        //Turns off the shield
        shield.SetActive(false);
        //Checks if the shield can be turned back on
        Invoke("CheckTurnOn", 1f);
    }

}
