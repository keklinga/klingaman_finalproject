﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
    //Sets up the singleton pattern
    public static GameController gc;
    //Access the score variable
    public int scoreFromPlanets = 0;
    //Access the score text
    public TextMeshProUGUI scoreText;
    //Access the lose text
    public TextMeshProUGUI loseText;
    //Access the lives text
    public TextMeshProUGUI livesText;
    //Access the lives of the player
    public int lives;
    //Access to the instruction panel
    public GameObject instructionPanel;
    //Access to the win text
    public TextMeshProUGUI winText;
    //Stores how many planets can spawn
    public int numToSpawn;
    //Stores how many planets the player destroyed
    public int numOfPlanetsDestroyed;


    void Awake()
    {
        //Checks if the gamecontroller is null
        if(gc == null)
        {
            //If it is, set it to be this gamecontroller
            gc = this;
            Time.timeScale = 0f;
        } else
        {
            //otherwise destroy it
            Destroy(gameObject);
        }

      
    }
    void Start()
    {
        //Initializes the score
        scoreFromPlanets = 0;
        //Initializes the lives
        lives = 4;
        //Initializes the score text
        SetScoreText();
        //Initializes the lives text
        SetLivesText();

    }

    // Update is called once per frame
    void Update()
    {
        //Checks if the Tab button has been pressed
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            //Unfreezes the game
            Time.timeScale = 1f;
            //Disables the instruction panel 
            instructionPanel.SetActive(false);
        }
        //Checks if we have destroyed all the planets
        if (numOfPlanetsDestroyed == numToSpawn)
        {
            //Sets the win text if all planets are destroyed
            SetWinText();
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    //Sets the score text on the screen
    public void SetScoreText()
    {
        //Sets the score text to be whatever the score is currently
        scoreText.text = "Score: " + scoreFromPlanets.ToString();
    }

    //Method to set lose text
    public void SetLoseText()
    {
        //enables the text gameobject
        loseText.gameObject.SetActive(true);
        //sets the actual text for the lose text
        loseText.text = "You are out of meteors! Press Enter and restart the game to play again.";
    }

    //Method to set the lives text 
    public void SetLivesText()
    {
        //Sets the lives text to how many meteors the player has left
        livesText.text = "Meteors Remaining: " + (lives).ToString();
    }

    //Method to set the win text
    public void SetWinText()
    {
        //enables winText if the player won
        winText.gameObject.SetActive(true);
        //Sets the text for win text
        winText.text = "You have conquered the Universe!";
    }
}
