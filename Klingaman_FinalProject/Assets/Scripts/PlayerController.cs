﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //Access the player's rigidbody
    public Rigidbody2D rb;
    //Access the hook for the player to attach to
    public Rigidbody2D hook;
    //Stores the time that the meteor is released from spring joint
    public float releaseTime = .15f;
    //The force that the meteor gets applied to it once it hits a planet
    public float meteorForce;
    //Stores a boolean to check if the mouse is being clicked
    private bool isClicked = false;
    //checks if we have launched the meteor
    private bool hasLaunched = false;
    //maximum launch drag distance
    public float maxDragDist = 1f;
    //Access the explosion particle system
    public GameObject planetExplosionEffect;
    //Access the next meteor to spawn
    public GameObject nextMeteor;
    //Stores the combo multiplier for the player score
    float comboMultliplier = 1f;
    //stores the amount of time the player has to get a combo score
    float comboTimerLength = 1.5f;
    //stores the timer that will use the combo timer length to check if they still have a combo timer active
    float comboTimer = 0f;
    //Access the explosion sound effect
    public AudioSource audioData;

    void Update()
    {
        //Checks if the mouse is being clicked
        if (isClicked)
        {
            //Sets a vector 2 to where the player follows where the mouse is being clicked
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Checks if the distance is greater than how far the player can drag the meteor
            if (Vector3.Distance(mousePos, hook.position) > maxDragDist)
                //sets the posotion to be where the player is dragging the player
                rb.position = hook.position + (mousePos - hook.position).normalized * maxDragDist;
            else
                //If the mouse is clicked, set the position of the meteor to where the mouse is going
                rb.position = mousePos;


        }

        //Checks if the planet has almost stopped and if it has launched
        if (rb.velocity.magnitude < .07f && hasLaunched)
        {
            //If it has, destroy the meteor
            Destroy(gameObject);
            //Decrease lives by 1
            GameController.gc.lives -= 1;
            //Load the next meteor onto the hook
            loadNextMeteor();
            //Adjust the lives text on the screen for the player
            GameController.gc.SetLivesText();
        }

        
        //Checks if the combo timer is greater than 0
        if (comboTimer > 0f)
        {
            //Subtract time from timer
            comboTimer -= Time.deltaTime;
            //Ran out of combo time
            if (comboTimer < 0)
            {
                //Reset back to normal multiplier
                comboMultliplier = 1f;
            }
        }
    }

    void OnMouseDown()
    {
        if (!hasLaunched)
        {
            //If mouse is being clicked, change the boolean to true
            isClicked = true;
            //Set the rigidbody to kinematic
            rb.isKinematic = true;
        }
    }

    void OnMouseUp()
    {
        //If mouse isn't being clicked, change boolean to false
        isClicked = false;
        //Disable kinematic property
        rb.isKinematic = false;
        //Start the release process after letting go of the mouse
        StartCoroutine(Release());
        
        
    }
    

    //Function to release the player from spring joint
    IEnumerator Release()
    {
        //Waits the amount of time decided by the variable releaseTime
        yield return new WaitForSeconds(releaseTime);
        //Disables the springjoint component
        GetComponent<SpringJoint2D>().enabled = false;
        //Reassigns the hasLaunched bool to true
        hasLaunched = true;
        

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Checks if we collided with a planet
        if (collision.gameObject.CompareTag("Planet"))
        {
            //Adds force to the meteor after colliding
            rb.AddForce(new Vector2(meteorForce, meteorForce));
            
            //Adds particles where and when a planet is destroyed
            Instantiate(planetExplosionEffect, transform.position, Quaternion.identity);
            //Plays the explosion sound effect when planet gets destroyed
            audioData.Play();

            //Disables the planets after being collided
            collision.gameObject.SetActive(false);
            //Increases everytime a planet is destroyed
            GameController.gc.numOfPlanetsDestroyed += 1;
            //Sets the score from destroying a planet
            ScorePlanet();
        }
        //Checks if the player hit the shield
        if (collision.gameObject.CompareTag("Shield"))
        {
            //Add force to the player and do not destroy the planet
            rb.AddForce(new Vector2(meteorForce, meteorForce));
        }
    }

    //Method to load in next meteor
    void loadNextMeteor()
    {
        //Checks if the reference on the meteor is not empty
        if (nextMeteor != null)
        {
            //Checks if the player has destroyed all available planets
            if(GameController.gc.numOfPlanetsDestroyed == GameController.gc.numToSpawn)
            {
                //If this is true, then do not activate the next meteor
                nextMeteor.SetActive(false);
            } else
            {
                //Otherwise, Sets the next meteor to be enabled
                nextMeteor.SetActive(true);
            }
            
        }
        else
        {
            //Else we set the lose text when out of lives
            GameController.gc.SetLoseText();
            
        }
    }

    //Method to adjust score and score multiplier of the player
    void ScorePlanet()
    {
        //Gets the score from the game controller script and assigns it to be 100 * the current multiplier
        GameController.gc.scoreFromPlanets += Mathf.RoundToInt(100f * comboMultliplier);
        //Assigns comboMultiplier to be 1.5x what it is
        comboMultliplier = comboMultliplier * 1.5f;
        //Set the timer to give the player time to increase their combo again
        comboTimer = comboTimerLength;
        //Updates the score text for the player
        GameController.gc.SetScoreText();
    }

}
